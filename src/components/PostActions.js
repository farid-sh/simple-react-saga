import {
  FETCH_POST_LOADING,
  FETCH_POST_SUCCESS,
  FETCH_POST_FAILURE,
} from "./PostTypes";
import axios from "axios";

export const fetchPostLoading = () => {
  return {
    type: FETCH_POST_LOADING,
  };
};

export const fetchPostSuccess = (Posts) => {
  return {
    type: FETCH_POST_SUCCESS,
    Posts,
  };
};

export const fetchPostFailure = (error) => {
  return {
    type: FETCH_POST_FAILURE,
    error: error,
  };
};

export const fetchPosts = async () => {
  try {
    const response = await axios.get(
      "https://jsonplaceholder.typicode.com/posts"
    );
    return response.data;
  } catch (error) {
    alert(error);
  }
};

// with fetch

// export const fetchPosts = async () => {
//   try {
//     const response = await fetch('https://jsonplaceholder.typicode.com/posts')
//     return response.data ;
//   } catch (error) {
//     alert("hooooooooooo")
//   }
// }

// with thunk

// export const fetchPosts =  () => {
//   return (dispatch) => {
//     dispatch(fetchPostLoading());
//     axios
//       .get("https://jsonplaceholder.typicode.com/posts")
//       .then((response) => {
//           const Posts= response.data;
//         dispatch(fetchPostSuccess(Posts));
//       })
//       .catch((error) => {
//         dispatch(fetchPostFailure(error.message));
//       });
//   };
// };
