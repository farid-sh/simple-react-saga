import React, { useEffect } from "react";
import { connect } from "react-redux";
import { fetchPostLoading } from "./PostActions";


function PostContainer({ postData, fetchPostLoading }) {


    useEffect(() => {
      fetchPostLoading();
    }, []);


    return postData.loading ? (
      <h2>Loading ... </h2>
    ) : postData.error ? (
      <h2>{postData.error}</h2>
    ) : (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div>
          {postData &&
            postData.Posts &&
            postData.Posts.map((post) => (
              <div key={post.id}>
                {" "}
                <p> id = {post.id}</p> <p>title = {post.title}</p>{" "}
                <p> body = {post.body}</p>{" "}
              </div>
            ))}
        </div>
      </div>
    );
  }
  
  const mapStateToProps = (state) => {
    return {
      postData: state.post,
    };
  };
  
  const mapDispatchToProps = (dispatch) => {
    return {
      fetchPostLoading: () => dispatch(fetchPostLoading()),
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(PostContainer);
