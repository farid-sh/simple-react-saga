import { combineReducers } from "redux";
import AllPostsReducer from './AllPostsReducer';


const rootReducer = combineReducers({
    post : AllPostsReducer,
});

export default rootReducer;