import {
    FETCH_POST_LOADING,
    FETCH_POST_SUCCESS,
    FETCH_POST_FAILURE,
  } from "./PostTypes";
  
  const initialState = {
    loading: false,
    Posts: [],
    error: "",
  };
  
  const reducer = (state = initialState, action) => {
    switch (action.type) {
      case FETCH_POST_LOADING:
        return {
          ...state,
          loading: true,
        };
      case FETCH_POST_SUCCESS:
        return {
          loading: false,
          Posts: action.Posts,
          error: "",
        };
      case FETCH_POST_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.error,
        };
      default:
        return state;
    }
  };
  
  export default reducer;
  