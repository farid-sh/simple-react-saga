import { createStore, applyMiddleware } from "redux";
// import thunk from 'redux-thunk';
import rootReducer from "./rootReducer";
import sagaMiddleware from 'redux-saga';
import mySaga from './saga';

const saga = sagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(saga));

saga.run(mySaga);

export default store;