import { takeLatest, call, put } from "redux-saga/effects";
import { fetchPosts, fetchPostSuccess, fetchPostFailure } from "./PostActions";
import { FETCH_POST_LOADING } from "./PostTypes";

function* getApiData() {
  try {
    const data = yield call(fetchPosts);
    yield put(fetchPostSuccess(data));
  } catch (error) {
    yield put(fetchPostFailure(error));
  }
}

export default function* mySaga() {
  yield takeLatest(FETCH_POST_LOADING, getApiData);
}