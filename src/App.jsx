import React from 'react';
import PostContainer from './components/PostContainer'
import store from './components/store'
import { Provider } from "react-redux";

function App() {
  return (
    <Provider store={store}>
    <div className="app">
    <PostContainer />
    </div>
    </Provider>
  );
}

export default App;